﻿using MaterialSkin;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace POOCatedra_Escuela
{
    public partial class FrmSplashScreen : MaterialSkin.Controls.MaterialForm
    {
        public FrmSplashScreen()
        {
            InitializeComponent();

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
                Primary.Green600, 
                Primary.BlueGrey900, 
                Primary.BlueGrey500, 
                Accent.LightBlue200, TextShade.WHITE);
        }

        private void FrmSplashScreen_Load(object sender, EventArgs e){}

        private void timerSplashScreen_Tick(object sender, EventArgs e)
        {
            pgb_loader.Increment(1);
            if(pgb_loader.Value == 100)
            {
                timerSplashScreen.Stop();
                FrmLogin login = new FrmLogin();
                login.Show();
                this.Hide();
            }

        }
    }
}
