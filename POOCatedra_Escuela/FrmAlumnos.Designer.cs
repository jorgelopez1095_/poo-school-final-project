﻿namespace POOCatedra_Escuela
{
    partial class FrmAlumnos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAlumnos));
            this.btn_back = new MaterialSkin.Controls.MaterialFlatButton();
            this.txtbx_codigo_alumno = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtbx_nombres_alumno = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtbx_apellidos_alumno = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtbx_direccion_alumno = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.lbl_codigo = new MaterialSkin.Controls.MaterialLabel();
            this.lbl_nombres = new MaterialSkin.Controls.MaterialLabel();
            this.lbl_apellidos = new MaterialSkin.Controls.MaterialLabel();
            this.lbl_direccion = new MaterialSkin.Controls.MaterialLabel();
            this.lbl_sexo = new MaterialSkin.Controls.MaterialLabel();
            this.lbl_telefono_casa = new MaterialSkin.Controls.MaterialLabel();
            this.lbl_telefono_celular = new MaterialSkin.Controls.MaterialLabel();
            this.lbl_fecha_nacimiento = new MaterialSkin.Controls.MaterialLabel();
            this.lbl_religion = new MaterialSkin.Controls.MaterialLabel();
            this.lbl_institucion_anterior = new MaterialSkin.Controls.MaterialLabel();
            this.lbl_responsable = new MaterialSkin.Controls.MaterialLabel();
            this.lbl_telefono_responsable = new MaterialSkin.Controls.MaterialLabel();
            this.cbx_sexo_alumno = new System.Windows.Forms.ComboBox();
            this.txtbx_telefono_casa_alumno = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtbx_telefono_responsable_alumno = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtbx_institucion_anterior = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtbx_religion_alumno = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtbx_fecha_nacimiento_alumno = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtbx_telefono_celular = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtbx_responsable_alumno = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.btn_agregar = new MaterialSkin.Controls.MaterialRaisedButton();
            this.dgv_alumnos = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_alumnos)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_back
            // 
            this.btn_back.AutoSize = true;
            this.btn_back.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btn_back.Depth = 0;
            this.btn_back.Location = new System.Drawing.Point(22, 314);
            this.btn_back.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btn_back.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_back.Name = "btn_back";
            this.btn_back.Primary = false;
            this.btn_back.Size = new System.Drawing.Size(80, 36);
            this.btn_back.TabIndex = 0;
            this.btn_back.Text = "REGRESAR";
            this.btn_back.UseVisualStyleBackColor = true;
            this.btn_back.Click += new System.EventHandler(this.btn_back_Click);
            // 
            // txtbx_codigo_alumno
            // 
            this.txtbx_codigo_alumno.Depth = 0;
            this.txtbx_codigo_alumno.Hint = "PP00000";
            this.txtbx_codigo_alumno.Location = new System.Drawing.Point(133, 80);
            this.txtbx_codigo_alumno.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtbx_codigo_alumno.Name = "txtbx_codigo_alumno";
            this.txtbx_codigo_alumno.PasswordChar = '\0';
            this.txtbx_codigo_alumno.SelectedText = "";
            this.txtbx_codigo_alumno.SelectionLength = 0;
            this.txtbx_codigo_alumno.SelectionStart = 0;
            this.txtbx_codigo_alumno.Size = new System.Drawing.Size(196, 23);
            this.txtbx_codigo_alumno.TabIndex = 1;
            this.txtbx_codigo_alumno.UseSystemPasswordChar = false;
            // 
            // txtbx_nombres_alumno
            // 
            this.txtbx_nombres_alumno.Depth = 0;
            this.txtbx_nombres_alumno.Hint = "Juan Jose";
            this.txtbx_nombres_alumno.Location = new System.Drawing.Point(133, 113);
            this.txtbx_nombres_alumno.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtbx_nombres_alumno.Name = "txtbx_nombres_alumno";
            this.txtbx_nombres_alumno.PasswordChar = '\0';
            this.txtbx_nombres_alumno.SelectedText = "";
            this.txtbx_nombres_alumno.SelectionLength = 0;
            this.txtbx_nombres_alumno.SelectionStart = 0;
            this.txtbx_nombres_alumno.Size = new System.Drawing.Size(196, 23);
            this.txtbx_nombres_alumno.TabIndex = 2;
            this.txtbx_nombres_alumno.UseSystemPasswordChar = false;
            // 
            // txtbx_apellidos_alumno
            // 
            this.txtbx_apellidos_alumno.Depth = 0;
            this.txtbx_apellidos_alumno.Hint = "Perez Sanchez";
            this.txtbx_apellidos_alumno.Location = new System.Drawing.Point(133, 146);
            this.txtbx_apellidos_alumno.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtbx_apellidos_alumno.Name = "txtbx_apellidos_alumno";
            this.txtbx_apellidos_alumno.PasswordChar = '\0';
            this.txtbx_apellidos_alumno.SelectedText = "";
            this.txtbx_apellidos_alumno.SelectionLength = 0;
            this.txtbx_apellidos_alumno.SelectionStart = 0;
            this.txtbx_apellidos_alumno.Size = new System.Drawing.Size(196, 23);
            this.txtbx_apellidos_alumno.TabIndex = 3;
            this.txtbx_apellidos_alumno.UseSystemPasswordChar = false;
            // 
            // txtbx_direccion_alumno
            // 
            this.txtbx_direccion_alumno.Depth = 0;
            this.txtbx_direccion_alumno.Hint = "San Salvador";
            this.txtbx_direccion_alumno.Location = new System.Drawing.Point(133, 179);
            this.txtbx_direccion_alumno.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtbx_direccion_alumno.Name = "txtbx_direccion_alumno";
            this.txtbx_direccion_alumno.PasswordChar = '\0';
            this.txtbx_direccion_alumno.SelectedText = "";
            this.txtbx_direccion_alumno.SelectionLength = 0;
            this.txtbx_direccion_alumno.SelectionStart = 0;
            this.txtbx_direccion_alumno.Size = new System.Drawing.Size(196, 23);
            this.txtbx_direccion_alumno.TabIndex = 4;
            this.txtbx_direccion_alumno.UseSystemPasswordChar = false;
            // 
            // lbl_codigo
            // 
            this.lbl_codigo.AutoSize = true;
            this.lbl_codigo.BackColor = System.Drawing.Color.Transparent;
            this.lbl_codigo.Depth = 0;
            this.lbl_codigo.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_codigo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_codigo.Location = new System.Drawing.Point(62, 84);
            this.lbl_codigo.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_codigo.Name = "lbl_codigo";
            this.lbl_codigo.Size = new System.Drawing.Size(65, 19);
            this.lbl_codigo.TabIndex = 6;
            this.lbl_codigo.Text = "Código: ";
            // 
            // lbl_nombres
            // 
            this.lbl_nombres.AutoSize = true;
            this.lbl_nombres.BackColor = System.Drawing.Color.Transparent;
            this.lbl_nombres.Depth = 0;
            this.lbl_nombres.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_nombres.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_nombres.Location = new System.Drawing.Point(48, 117);
            this.lbl_nombres.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_nombres.Name = "lbl_nombres";
            this.lbl_nombres.Size = new System.Drawing.Size(79, 19);
            this.lbl_nombres.TabIndex = 7;
            this.lbl_nombres.Text = "Nombres: ";
            // 
            // lbl_apellidos
            // 
            this.lbl_apellidos.AutoSize = true;
            this.lbl_apellidos.BackColor = System.Drawing.Color.Transparent;
            this.lbl_apellidos.Depth = 0;
            this.lbl_apellidos.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_apellidos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_apellidos.Location = new System.Drawing.Point(47, 150);
            this.lbl_apellidos.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_apellidos.Name = "lbl_apellidos";
            this.lbl_apellidos.Size = new System.Drawing.Size(80, 19);
            this.lbl_apellidos.TabIndex = 8;
            this.lbl_apellidos.Text = "Apellidos: ";
            // 
            // lbl_direccion
            // 
            this.lbl_direccion.AutoSize = true;
            this.lbl_direccion.BackColor = System.Drawing.Color.Transparent;
            this.lbl_direccion.Depth = 0;
            this.lbl_direccion.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_direccion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_direccion.Location = new System.Drawing.Point(46, 183);
            this.lbl_direccion.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_direccion.Name = "lbl_direccion";
            this.lbl_direccion.Size = new System.Drawing.Size(81, 19);
            this.lbl_direccion.TabIndex = 9;
            this.lbl_direccion.Text = "Dirección: ";
            // 
            // lbl_sexo
            // 
            this.lbl_sexo.AutoSize = true;
            this.lbl_sexo.BackColor = System.Drawing.Color.Transparent;
            this.lbl_sexo.Depth = 0;
            this.lbl_sexo.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_sexo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_sexo.Location = new System.Drawing.Point(77, 214);
            this.lbl_sexo.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_sexo.Name = "lbl_sexo";
            this.lbl_sexo.Size = new System.Drawing.Size(50, 19);
            this.lbl_sexo.TabIndex = 10;
            this.lbl_sexo.Text = "Sexo: ";
            // 
            // lbl_telefono_casa
            // 
            this.lbl_telefono_casa.AutoSize = true;
            this.lbl_telefono_casa.BackColor = System.Drawing.Color.Transparent;
            this.lbl_telefono_casa.Depth = 0;
            this.lbl_telefono_casa.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_telefono_casa.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_telefono_casa.Location = new System.Drawing.Point(18, 247);
            this.lbl_telefono_casa.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_telefono_casa.Name = "lbl_telefono_casa";
            this.lbl_telefono_casa.Size = new System.Drawing.Size(109, 19);
            this.lbl_telefono_casa.TabIndex = 11;
            this.lbl_telefono_casa.Text = "Telefono casa:";
            // 
            // lbl_telefono_celular
            // 
            this.lbl_telefono_celular.AutoSize = true;
            this.lbl_telefono_celular.BackColor = System.Drawing.Color.Transparent;
            this.lbl_telefono_celular.Depth = 0;
            this.lbl_telefono_celular.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_telefono_celular.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_telefono_celular.Location = new System.Drawing.Point(414, 84);
            this.lbl_telefono_celular.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_telefono_celular.Name = "lbl_telefono_celular";
            this.lbl_telefono_celular.Size = new System.Drawing.Size(126, 19);
            this.lbl_telefono_celular.TabIndex = 12;
            this.lbl_telefono_celular.Text = "Telefono celular: ";
            // 
            // lbl_fecha_nacimiento
            // 
            this.lbl_fecha_nacimiento.AutoSize = true;
            this.lbl_fecha_nacimiento.BackColor = System.Drawing.Color.Transparent;
            this.lbl_fecha_nacimiento.Depth = 0;
            this.lbl_fecha_nacimiento.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_fecha_nacimiento.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_fecha_nacimiento.Location = new System.Drawing.Point(408, 117);
            this.lbl_fecha_nacimiento.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_fecha_nacimiento.Name = "lbl_fecha_nacimiento";
            this.lbl_fecha_nacimiento.Size = new System.Drawing.Size(132, 19);
            this.lbl_fecha_nacimiento.TabIndex = 13;
            this.lbl_fecha_nacimiento.Text = "Fecha nacimiento:";
            // 
            // lbl_religion
            // 
            this.lbl_religion.AutoSize = true;
            this.lbl_religion.BackColor = System.Drawing.Color.Transparent;
            this.lbl_religion.Depth = 0;
            this.lbl_religion.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_religion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_religion.Location = new System.Drawing.Point(469, 150);
            this.lbl_religion.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_religion.Name = "lbl_religion";
            this.lbl_religion.Size = new System.Drawing.Size(71, 19);
            this.lbl_religion.TabIndex = 14;
            this.lbl_religion.Text = "Religión: ";
            // 
            // lbl_institucion_anterior
            // 
            this.lbl_institucion_anterior.AutoSize = true;
            this.lbl_institucion_anterior.BackColor = System.Drawing.Color.Transparent;
            this.lbl_institucion_anterior.Depth = 0;
            this.lbl_institucion_anterior.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_institucion_anterior.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_institucion_anterior.Location = new System.Drawing.Point(400, 183);
            this.lbl_institucion_anterior.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_institucion_anterior.Name = "lbl_institucion_anterior";
            this.lbl_institucion_anterior.Size = new System.Drawing.Size(140, 19);
            this.lbl_institucion_anterior.TabIndex = 15;
            this.lbl_institucion_anterior.Text = "Institucion anterior:";
            // 
            // lbl_responsable
            // 
            this.lbl_responsable.AutoSize = true;
            this.lbl_responsable.BackColor = System.Drawing.Color.Transparent;
            this.lbl_responsable.Depth = 0;
            this.lbl_responsable.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_responsable.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_responsable.Location = new System.Drawing.Point(437, 216);
            this.lbl_responsable.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_responsable.Name = "lbl_responsable";
            this.lbl_responsable.Size = new System.Drawing.Size(103, 19);
            this.lbl_responsable.TabIndex = 16;
            this.lbl_responsable.Text = "Responsable: ";
            // 
            // lbl_telefono_responsable
            // 
            this.lbl_telefono_responsable.AutoSize = true;
            this.lbl_telefono_responsable.BackColor = System.Drawing.Color.Transparent;
            this.lbl_telefono_responsable.Depth = 0;
            this.lbl_telefono_responsable.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_telefono_responsable.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_telefono_responsable.Location = new System.Drawing.Point(381, 249);
            this.lbl_telefono_responsable.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_telefono_responsable.Name = "lbl_telefono_responsable";
            this.lbl_telefono_responsable.Size = new System.Drawing.Size(159, 19);
            this.lbl_telefono_responsable.TabIndex = 17;
            this.lbl_telefono_responsable.Text = "Teléfono responsable:";
            // 
            // cbx_sexo_alumno
            // 
            this.cbx_sexo_alumno.FormattingEnabled = true;
            this.cbx_sexo_alumno.Items.AddRange(new object[] {
            "Masculino",
            "Femenino"});
            this.cbx_sexo_alumno.Location = new System.Drawing.Point(133, 212);
            this.cbx_sexo_alumno.Name = "cbx_sexo_alumno";
            this.cbx_sexo_alumno.Size = new System.Drawing.Size(196, 21);
            this.cbx_sexo_alumno.TabIndex = 18;
            // 
            // txtbx_telefono_casa_alumno
            // 
            this.txtbx_telefono_casa_alumno.Depth = 0;
            this.txtbx_telefono_casa_alumno.Hint = "xxxx-xxxx";
            this.txtbx_telefono_casa_alumno.Location = new System.Drawing.Point(133, 243);
            this.txtbx_telefono_casa_alumno.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtbx_telefono_casa_alumno.Name = "txtbx_telefono_casa_alumno";
            this.txtbx_telefono_casa_alumno.PasswordChar = '\0';
            this.txtbx_telefono_casa_alumno.SelectedText = "";
            this.txtbx_telefono_casa_alumno.SelectionLength = 0;
            this.txtbx_telefono_casa_alumno.SelectionStart = 0;
            this.txtbx_telefono_casa_alumno.Size = new System.Drawing.Size(196, 23);
            this.txtbx_telefono_casa_alumno.TabIndex = 19;
            this.txtbx_telefono_casa_alumno.UseSystemPasswordChar = false;
            // 
            // txtbx_telefono_responsable_alumno
            // 
            this.txtbx_telefono_responsable_alumno.Depth = 0;
            this.txtbx_telefono_responsable_alumno.Hint = "xxxx-xxxx";
            this.txtbx_telefono_responsable_alumno.Location = new System.Drawing.Point(546, 245);
            this.txtbx_telefono_responsable_alumno.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtbx_telefono_responsable_alumno.Name = "txtbx_telefono_responsable_alumno";
            this.txtbx_telefono_responsable_alumno.PasswordChar = '\0';
            this.txtbx_telefono_responsable_alumno.SelectedText = "";
            this.txtbx_telefono_responsable_alumno.SelectionLength = 0;
            this.txtbx_telefono_responsable_alumno.SelectionStart = 0;
            this.txtbx_telefono_responsable_alumno.Size = new System.Drawing.Size(196, 23);
            this.txtbx_telefono_responsable_alumno.TabIndex = 25;
            this.txtbx_telefono_responsable_alumno.UseSystemPasswordChar = false;
            // 
            // txtbx_institucion_anterior
            // 
            this.txtbx_institucion_anterior.Depth = 0;
            this.txtbx_institucion_anterior.Hint = "Escuela X";
            this.txtbx_institucion_anterior.Location = new System.Drawing.Point(546, 179);
            this.txtbx_institucion_anterior.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtbx_institucion_anterior.Name = "txtbx_institucion_anterior";
            this.txtbx_institucion_anterior.PasswordChar = '\0';
            this.txtbx_institucion_anterior.SelectedText = "";
            this.txtbx_institucion_anterior.SelectionLength = 0;
            this.txtbx_institucion_anterior.SelectionStart = 0;
            this.txtbx_institucion_anterior.Size = new System.Drawing.Size(196, 23);
            this.txtbx_institucion_anterior.TabIndex = 23;
            this.txtbx_institucion_anterior.UseSystemPasswordChar = false;
            // 
            // txtbx_religion_alumno
            // 
            this.txtbx_religion_alumno.Depth = 0;
            this.txtbx_religion_alumno.Hint = "Religión";
            this.txtbx_religion_alumno.Location = new System.Drawing.Point(546, 146);
            this.txtbx_religion_alumno.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtbx_religion_alumno.Name = "txtbx_religion_alumno";
            this.txtbx_religion_alumno.PasswordChar = '\0';
            this.txtbx_religion_alumno.SelectedText = "";
            this.txtbx_religion_alumno.SelectionLength = 0;
            this.txtbx_religion_alumno.SelectionStart = 0;
            this.txtbx_religion_alumno.Size = new System.Drawing.Size(196, 23);
            this.txtbx_religion_alumno.TabIndex = 22;
            this.txtbx_religion_alumno.UseSystemPasswordChar = false;
            // 
            // txtbx_fecha_nacimiento_alumno
            // 
            this.txtbx_fecha_nacimiento_alumno.Depth = 0;
            this.txtbx_fecha_nacimiento_alumno.Hint = "YYYY-MM-DD";
            this.txtbx_fecha_nacimiento_alumno.Location = new System.Drawing.Point(546, 113);
            this.txtbx_fecha_nacimiento_alumno.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtbx_fecha_nacimiento_alumno.Name = "txtbx_fecha_nacimiento_alumno";
            this.txtbx_fecha_nacimiento_alumno.PasswordChar = '\0';
            this.txtbx_fecha_nacimiento_alumno.SelectedText = "";
            this.txtbx_fecha_nacimiento_alumno.SelectionLength = 0;
            this.txtbx_fecha_nacimiento_alumno.SelectionStart = 0;
            this.txtbx_fecha_nacimiento_alumno.Size = new System.Drawing.Size(196, 23);
            this.txtbx_fecha_nacimiento_alumno.TabIndex = 21;
            this.txtbx_fecha_nacimiento_alumno.UseSystemPasswordChar = false;
            // 
            // txtbx_telefono_celular
            // 
            this.txtbx_telefono_celular.Depth = 0;
            this.txtbx_telefono_celular.Hint = "xxxx-xxxx";
            this.txtbx_telefono_celular.Location = new System.Drawing.Point(546, 80);
            this.txtbx_telefono_celular.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtbx_telefono_celular.Name = "txtbx_telefono_celular";
            this.txtbx_telefono_celular.PasswordChar = '\0';
            this.txtbx_telefono_celular.SelectedText = "";
            this.txtbx_telefono_celular.SelectionLength = 0;
            this.txtbx_telefono_celular.SelectionStart = 0;
            this.txtbx_telefono_celular.Size = new System.Drawing.Size(196, 23);
            this.txtbx_telefono_celular.TabIndex = 20;
            this.txtbx_telefono_celular.UseSystemPasswordChar = false;
            // 
            // txtbx_responsable_alumno
            // 
            this.txtbx_responsable_alumno.Depth = 0;
            this.txtbx_responsable_alumno.Hint = "Papá/Mamá/...";
            this.txtbx_responsable_alumno.Location = new System.Drawing.Point(546, 212);
            this.txtbx_responsable_alumno.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtbx_responsable_alumno.Name = "txtbx_responsable_alumno";
            this.txtbx_responsable_alumno.PasswordChar = '\0';
            this.txtbx_responsable_alumno.SelectedText = "";
            this.txtbx_responsable_alumno.SelectionLength = 0;
            this.txtbx_responsable_alumno.SelectionStart = 0;
            this.txtbx_responsable_alumno.Size = new System.Drawing.Size(196, 23);
            this.txtbx_responsable_alumno.TabIndex = 26;
            this.txtbx_responsable_alumno.UseSystemPasswordChar = false;
            // 
            // btn_agregar
            // 
            this.btn_agregar.Depth = 0;
            this.btn_agregar.Location = new System.Drawing.Point(546, 288);
            this.btn_agregar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_agregar.Name = "btn_agregar";
            this.btn_agregar.Primary = true;
            this.btn_agregar.Size = new System.Drawing.Size(196, 46);
            this.btn_agregar.TabIndex = 27;
            this.btn_agregar.Text = "AGREGAR";
            this.btn_agregar.UseVisualStyleBackColor = true;
            this.btn_agregar.Click += new System.EventHandler(this.btn_agregar_Click);
            // 
            // dgv_alumnos
            // 
            this.dgv_alumnos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_alumnos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_alumnos.Location = new System.Drawing.Point(22, 359);
            this.dgv_alumnos.Name = "dgv_alumnos";
            this.dgv_alumnos.Size = new System.Drawing.Size(720, 263);
            this.dgv_alumnos.TabIndex = 28;
            // 
            // FrmAlumnos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(765, 634);
            this.Controls.Add(this.dgv_alumnos);
            this.Controls.Add(this.btn_agregar);
            this.Controls.Add(this.txtbx_responsable_alumno);
            this.Controls.Add(this.txtbx_telefono_responsable_alumno);
            this.Controls.Add(this.txtbx_institucion_anterior);
            this.Controls.Add(this.txtbx_religion_alumno);
            this.Controls.Add(this.txtbx_fecha_nacimiento_alumno);
            this.Controls.Add(this.txtbx_telefono_celular);
            this.Controls.Add(this.txtbx_telefono_casa_alumno);
            this.Controls.Add(this.cbx_sexo_alumno);
            this.Controls.Add(this.lbl_telefono_responsable);
            this.Controls.Add(this.lbl_responsable);
            this.Controls.Add(this.lbl_institucion_anterior);
            this.Controls.Add(this.lbl_religion);
            this.Controls.Add(this.lbl_fecha_nacimiento);
            this.Controls.Add(this.lbl_telefono_celular);
            this.Controls.Add(this.lbl_telefono_casa);
            this.Controls.Add(this.lbl_sexo);
            this.Controls.Add(this.lbl_direccion);
            this.Controls.Add(this.lbl_apellidos);
            this.Controls.Add(this.lbl_nombres);
            this.Controls.Add(this.lbl_codigo);
            this.Controls.Add(this.txtbx_direccion_alumno);
            this.Controls.Add(this.txtbx_apellidos_alumno);
            this.Controls.Add(this.txtbx_nombres_alumno);
            this.Controls.Add(this.txtbx_codigo_alumno);
            this.Controls.Add(this.btn_back);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmAlumnos";
            this.Text = "Alumnos";
            this.Load += new System.EventHandler(this.FrmAlumnos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_alumnos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialFlatButton btn_back;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtbx_codigo_alumno;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtbx_nombres_alumno;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtbx_apellidos_alumno;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtbx_direccion_alumno;
        private MaterialSkin.Controls.MaterialLabel lbl_codigo;
        private MaterialSkin.Controls.MaterialLabel lbl_nombres;
        private MaterialSkin.Controls.MaterialLabel lbl_apellidos;
        private MaterialSkin.Controls.MaterialLabel lbl_direccion;
        private MaterialSkin.Controls.MaterialLabel lbl_sexo;
        private MaterialSkin.Controls.MaterialLabel lbl_telefono_casa;
        private MaterialSkin.Controls.MaterialLabel lbl_telefono_celular;
        private MaterialSkin.Controls.MaterialLabel lbl_fecha_nacimiento;
        private MaterialSkin.Controls.MaterialLabel lbl_religion;
        private MaterialSkin.Controls.MaterialLabel lbl_institucion_anterior;
        private MaterialSkin.Controls.MaterialLabel lbl_responsable;
        private MaterialSkin.Controls.MaterialLabel lbl_telefono_responsable;
        private System.Windows.Forms.ComboBox cbx_sexo_alumno;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtbx_telefono_casa_alumno;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtbx_telefono_responsable_alumno;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtbx_institucion_anterior;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtbx_religion_alumno;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtbx_fecha_nacimiento_alumno;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtbx_telefono_celular;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtbx_responsable_alumno;
        private MaterialSkin.Controls.MaterialRaisedButton btn_agregar;
        private System.Windows.Forms.DataGridView dgv_alumnos;
    }
}