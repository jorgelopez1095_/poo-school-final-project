﻿namespace POOCatedra_Escuela
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.btn_alumnos = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btn_docentes = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btn_notas = new MaterialSkin.Controls.MaterialRaisedButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_alumnos
            // 
            this.btn_alumnos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_alumnos.Depth = 0;
            this.btn_alumnos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_alumnos.Location = new System.Drawing.Point(364, 121);
            this.btn_alumnos.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_alumnos.Name = "btn_alumnos";
            this.btn_alumnos.Primary = true;
            this.btn_alumnos.Size = new System.Drawing.Size(200, 48);
            this.btn_alumnos.TabIndex = 0;
            this.btn_alumnos.Text = "ALUMNOS";
            this.btn_alumnos.UseVisualStyleBackColor = true;
            this.btn_alumnos.Click += new System.EventHandler(this.btn_alumnos_Click);
            // 
            // btn_docentes
            // 
            this.btn_docentes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_docentes.Depth = 0;
            this.btn_docentes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_docentes.Location = new System.Drawing.Point(364, 189);
            this.btn_docentes.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_docentes.Name = "btn_docentes";
            this.btn_docentes.Primary = true;
            this.btn_docentes.Size = new System.Drawing.Size(200, 48);
            this.btn_docentes.TabIndex = 1;
            this.btn_docentes.Text = "DOCENTES";
            this.btn_docentes.UseVisualStyleBackColor = true;
            // 
            // btn_notas
            // 
            this.btn_notas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_notas.Depth = 0;
            this.btn_notas.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_notas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_notas.Location = new System.Drawing.Point(364, 261);
            this.btn_notas.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_notas.Name = "btn_notas";
            this.btn_notas.Primary = true;
            this.btn_notas.Size = new System.Drawing.Size(200, 48);
            this.btn_notas.TabIndex = 2;
            this.btn_notas.Text = "NOTAS";
            this.btn_notas.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(26, 121);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(309, 274);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(589, 31);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(24, 24);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 394);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btn_notas);
            this.Controls.Add(this.btn_docentes);
            this.Controls.Add(this.btn_alumnos);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(50)))), ((int)(((byte)(56)))));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMain";
            this.Text = "LICEO LEONARDO AZCUNAGA";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialRaisedButton btn_alumnos;
        private MaterialSkin.Controls.MaterialRaisedButton btn_docentes;
        private MaterialSkin.Controls.MaterialRaisedButton btn_notas;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}