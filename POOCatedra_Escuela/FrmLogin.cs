﻿using MaterialSkin;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace POOCatedra_Escuela
{
    public partial class FrmLogin : MaterialSkin.Controls.MaterialForm
    {
        public FrmLogin()
        {
            InitializeComponent();

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green600, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);

            txtbx_pwd.PasswordChar = '*';

        }

        private void FormLogin_Load(object sender, EventArgs e){}
        private void txtbx_pwd_Click(object sender, EventArgs e){}

        private void btn_login_Click(object sender, EventArgs e)
        {
            if (classes._Select.validateUser(txtbx_user.Text, txtbx_pwd.Text)) actionAfterLogin(1);
        }

        private void actionAfterLogin(int flag)
        {
            if (flag == 1){
                MessageBox.Show("BIENVENIDO");
                new FrmMain().Show();
                this.Hide();
            }

            else MessageBox.Show("Credenciales incorrectas, vuelve a intentar.");
        }
    }
}
