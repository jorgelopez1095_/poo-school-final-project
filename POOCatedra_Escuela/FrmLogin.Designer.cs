﻿namespace POOCatedra_Escuela
{
    partial class FrmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLogin));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btn_login = new MaterialSkin.Controls.MaterialRaisedButton();
            this.txtbx_user = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.txtbx_pwd = new MaterialSkin.Controls.MaterialSingleLineTextField();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(242, 96);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(167, 157);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btn_login
            // 
            this.btn_login.Depth = 0;
            this.btn_login.Location = new System.Drawing.Point(58, 185);
            this.btn_login.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_login.Name = "btn_login";
            this.btn_login.Primary = true;
            this.btn_login.Size = new System.Drawing.Size(125, 35);
            this.btn_login.TabIndex = 3;
            this.btn_login.Text = "ENTRAR";
            this.btn_login.UseVisualStyleBackColor = true;
            this.btn_login.Click += new System.EventHandler(this.btn_login_Click);
            // 
            // txtbx_user
            // 
            this.txtbx_user.Depth = 0;
            this.txtbx_user.Hint = "usuario";
            this.txtbx_user.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txtbx_user.Location = new System.Drawing.Point(36, 96);
            this.txtbx_user.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtbx_user.Name = "txtbx_user";
            this.txtbx_user.PasswordChar = '\0';
            this.txtbx_user.SelectedText = "";
            this.txtbx_user.SelectionLength = 0;
            this.txtbx_user.SelectionStart = 0;
            this.txtbx_user.Size = new System.Drawing.Size(167, 23);
            this.txtbx_user.TabIndex = 1;
            this.txtbx_user.UseSystemPasswordChar = false;
            // 
            // txtbx_pwd
            // 
            this.txtbx_pwd.Depth = 0;
            this.txtbx_pwd.Hint = "*******";
            this.txtbx_pwd.Location = new System.Drawing.Point(36, 140);
            this.txtbx_pwd.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtbx_pwd.Name = "txtbx_pwd";
            this.txtbx_pwd.PasswordChar = '\0';
            this.txtbx_pwd.SelectedText = "";
            this.txtbx_pwd.SelectionLength = 0;
            this.txtbx_pwd.SelectionStart = 0;
            this.txtbx_pwd.Size = new System.Drawing.Size(167, 23);
            this.txtbx_pwd.TabIndex = 2;
            this.txtbx_pwd.UseSystemPasswordChar = false;
            this.txtbx_pwd.Click += new System.EventHandler(this.txtbx_pwd_Click);
            // 
            // FrmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(421, 251);
            this.Controls.Add(this.btn_login);
            this.Controls.Add(this.txtbx_pwd);
            this.Controls.Add(this.txtbx_user);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Inicio de sesión";
            this.Load += new System.EventHandler(this.FormLogin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private MaterialSkin.Controls.MaterialRaisedButton btn_login;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtbx_user;
        private MaterialSkin.Controls.MaterialSingleLineTextField txtbx_pwd;
    }
}