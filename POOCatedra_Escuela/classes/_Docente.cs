﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POOCatedra_Escuela.classes
{
    class _Docente
    {
        public string codigo_docente    { get; set; }
        public string nombres           { get; set; }
        public string apellidos         { get; set; }
        public string direccion         { get; set; }
        public string sexo              { get; set; }
        public string telefono          { get; set; }
        public string fecha_nacimiento  { get; set; }
        public string dui               { get; set; }
        public string nit               { get; set; }
        public string correo_electronico { get; set;}
	    public string fecha_ingreso     { get; set; }

        public _Docente(
            string codigo_docente,
            string nombres, 
            string apellidos, 
            string direccion, 
            string sexo, 
            string telefono, 
            string fecha_nacimiento, 
            string dui, 
            string nit, 
            string correo_electronico, 
            string fecha_ingreso)
        {
            this.codigo_docente     = codigo_docente;
            this.nombres            = nombres;
            this.apellidos          = apellidos;
            this.direccion          = direccion;
            this.sexo               = sexo;
            this.telefono           = telefono;
            this.fecha_nacimiento   = fecha_nacimiento;
            this.dui                = dui;
            this.nit                = nit;
            this.correo_electronico = correo_electronico;
            this.fecha_ingreso      = fecha_ingreso;
        }
    }

}