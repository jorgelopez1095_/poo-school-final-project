﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POOCatedra_Escuela.classes
{
    class _Insert
    {
        public static int agregarAlumno(classes._Alumno Alumno)
        {

        int flag = 0;


            using (SqlConnection connection = _Connection.dbConnection())
            {
                SqlCommand query = new SqlCommand(string.Format(
                "INSERT INTO Alumnos"   +
                "(  codigo_alumno, "              +
                    "nombres, "                 +
                    "apellidos, "               +
                    "direccion, "               +
                    "sexo, "                    +
                    "telefono_casa, "           +
                    "telefono_celular, "        +
                    "fecha_nacimiento, "        +
                    "edad, "                    +
                    "religion,"                 +
                    "institucion_anterior,"     +
                    "nombre_responsable, "      +
                    "telefono_responsable) "          +
                "VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}')",
                Alumno.codigo_alumno,           Alumno.nombres, 
                Alumno.apellidos,               Alumno.direccion, 
                Alumno.sexo,                    Alumno.telefono_casa,
                Alumno.telefono_celular,        Alumno.fecha_nacimiento,        
                Alumno.edad,                    Alumno.religion,
                Alumno.institucion_anterior,    Alumno.nombre_responsable, 
                Alumno.telefono_responsable), connection);

                flag = query.ExecuteNonQuery();

                connection.Close();
            }

            return flag;
        }

        public static int agregarDocente(classes._Docente Docente)
        {
            int flag = 0;

            using (SqlConnection connection = _Connection.dbConnection())
            {
                SqlCommand query = new SqlCommand(string.Format(
                "INSERT INTO escuela.Alumnos" +
                "(  codigo_docente, " +
                    "nombres, " +
                    "apellidos, " +
                    "direccion, " +
                    "sexo, " +
                    "telefono, " +
                    "fecha_nacimiento, " +
                    "dui, " +
                    "nit, " +
                    "correo_electronico," +
                    "fecha_ingreso) " +
                "VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}')",
                Docente.codigo_docente, Docente.nombres,
                Docente.apellidos, Docente.direccion,
                Docente.sexo, Docente.telefono,
                Docente.fecha_nacimiento, Docente.dui,
                Docente.nit, Docente.correo_electronico, Docente.fecha_ingreso), connection);

                flag = query.ExecuteNonQuery();

                connection.Close();
            }


            return flag;
        }
    }
}
