﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POOCatedra_Escuela.classes
{
    class _Select
    {

        public static Boolean validateUser(String user_code, String user_password)
        {
            Boolean flag = false;
            using (SqlConnection conn = _Connection.dbConnection())
            {
                SqlCommand validateUser = new SqlCommand(String.Format("SELECT * FROM Usuarios WHERE codigo_docente = '{0}' AND usr_password = '{1}'", user_code,user_password), conn);

                
                using (SqlDataReader reader = validateUser.ExecuteReader())
                {
                    flag = reader.Read();
                }

                conn.Close();
            }
            return flag;
        }
        
        
        public static List<_Alumno> getAlumnos()
        {
            List<_Alumno> alumnos = new List<_Alumno>();

            using (SqlConnection connection = _Connection.dbConnection())
            {
                SqlCommand query = new SqlCommand(String.Format("SELECT * FROM Alumnos"), connection);

                using (SqlDataReader reader = query.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        _Alumno pAlumno = new _Alumno();
                        pAlumno.codigo_alumno = reader.GetString(0);
                        pAlumno.nombres = reader.GetString(1);
                        pAlumno.apellidos = reader.GetString(2);
                        pAlumno.direccion = reader.GetString(3);
                        pAlumno.sexo = reader.GetString(4);
                        pAlumno.telefono_casa = reader.GetString(5);
                        pAlumno.telefono_celular = reader.GetString(6);
                        pAlumno.fecha_nacimiento = reader.GetDateTime(7);
                        pAlumno.edad = reader.GetInt32(8);
                        pAlumno.religion = reader.GetString(9);
                        pAlumno.institucion_anterior = reader.GetString(10);
                        pAlumno.nombre_responsable = reader.GetString(11);
                        pAlumno.telefono_responsable = reader.GetString(12);
                        
                        //pAlumno.fecha_creacion = reader.GetDateTime(13);

                        alumnos.Add(pAlumno);
                    }
                }

                connection.Close();
                return alumnos;
            }


            return null;
        }

    }
}
