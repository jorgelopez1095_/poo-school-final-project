﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POOCatedra_Escuela.classes
{
    class _Alumno
    {
        public string codigo_alumno         { get; set; }
        public string nombres               { get; set; }
        public string apellidos             { get; set; }
	    public string direccion             { get; set; }
	    public string sexo                  { get; set; }
        public string telefono_casa         { get; set; }
        public string telefono_celular      { get; set; }
        public DateTime fecha_nacimiento      { get; set; }
        public int edad                     { get; set; }
        public string religion              { get; set; }
        public string institucion_anterior  { get; set; }
        public string nombre_responsable    { get; set; }
    	public string telefono_responsable  { get; set; }
	    public DateTime fecha_creacion      { get; set; }

        public _Alumno(){}

        public _Alumno(
            string codigo_alumno, 
            string nombres, 
            string apellidos, 
            string direccion, 
            string sexo, 
            string telefono_casa, 
            string telefono_celular, 
            DateTime fecha_nacimiento, 
            int edad, 
            string religion,
            string institucion_anterior, 
            string nombre_responsable, 
            string telefono_responsable, 
            DateTime fecha_creacion)
        {
            this.codigo_alumno = codigo_alumno;
            this.nombres = nombres;
            this.apellidos = apellidos;
            this.direccion = direccion;
            this.sexo = sexo;
            this.telefono_casa = telefono_casa;
            this.telefono_celular = telefono_celular;
            this.fecha_nacimiento = fecha_nacimiento;
            this.edad = edad;
            this.religion = religion;
            this.institucion_anterior = institucion_anterior;
            this.nombre_responsable = nombre_responsable;
            this.telefono_responsable = telefono_responsable;
            this.fecha_creacion = fecha_creacion;
        }
    }
}
