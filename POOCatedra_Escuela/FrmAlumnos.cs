﻿using MaterialSkin;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace POOCatedra_Escuela
{
    public partial class FrmAlumnos : MaterialSkin.Controls.MaterialForm
    {
        public FrmAlumnos()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Green600, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            new FrmMain().Show();
            this.Hide();
        }

        private void FrmAlumnos_Load(object sender, EventArgs e)
        {
            setDataGridView();
        }

        private void btn_agregar_Click(object sender, EventArgs e)
        {
            classes._Alumno alumno = new classes._Alumno();
            alumno.codigo_alumno = txtbx_codigo_alumno.Text.Trim();
            alumno.nombres = txtbx_nombres_alumno.Text.Trim();
            alumno.direccion = txtbx_direccion_alumno.Text.Trim();
            alumno.sexo = "N";
            alumno.telefono_casa = txtbx_telefono_casa_alumno.Text.Trim();
            alumno.telefono_celular = txtbx_telefono_celular.Text.Trim();
            alumno.fecha_nacimiento = Convert.ToDateTime(txtbx_fecha_nacimiento_alumno.Text.Trim());
            alumno.religion = txtbx_religion_alumno.Text.Trim();
            alumno.institucion_anterior = txtbx_institucion_anterior.Text.Trim();
            alumno.nombre_responsable = txtbx_responsable_alumno.Text.Trim();
            alumno.telefono_responsable = txtbx_telefono_responsable_alumno.Text.Trim();

            int resultado = classes._Insert.agregarAlumno(alumno);
            if(resultado != 0)
            {
                MessageBox.Show("Alumno registrado con exito.");
                setDataGridView();
            }
            else
            {
                MessageBox.Show("Ocurrió un problema en la creacion de alumno");
            }

        }


        private void setDataGridView()
        {
            dgv_alumnos.DataSource = classes._Select.getAlumnos();
        }
    }
}
